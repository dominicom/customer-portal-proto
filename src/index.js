import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import App from "./App";

import "./styles.scss";
import "./components.scss";

//
// HTML code for components:
//

// Details View (details-view):
//
// @see  /src/HTMLcontents/details-view.html

// User Panel (user-panel):
//
// @see  /src/HTMLcontents/user-panel.html

// Upload File modal (upload-file):
//
// @see  /src/HTMLcontents/upload-file.html

const rootElement = document.getElementById("root");
ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  rootElement
);


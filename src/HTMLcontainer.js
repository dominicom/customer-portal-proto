import React from "react";



export default ({ render }) => {
  return (
    <div dangerouslySetInnerHTML={{ __html: render ? render : Placeholder }} />
  );
};

const Placeholder = `
  <p class="placeholder">Placeholder</p>
`;

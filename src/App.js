import React, { Component } from "react";

import { Redirect, Switch, Route, withRouter } from "react-router-dom";
import { PortalContext, ContextProvider } from 'ContextStore';

//import Console from "./console/Console";

// App assets
import UIShell from "components/elements/UIShell";
import DownloadPanel from "components/assets/Download";

// import EmptyState from "components/assets/EmptyState";

import Dashboard from "pages/Dashboard/Dashboard";
import Documents from "pages/Documents/Documents";
import Cases from "pages/Cases/Cases";
import Invoices from "pages/Invoices/Invoices";
import Tasks from "pages/Tasks/Tasks";

import Case from "pages/Cases/subpages/Case";

import Contact from "pages/Contact";
import PersonalData from "pages/PersonalData";


const settings = {
  isUploadWindowOpen: false
};

const routes = [
  {
    path: "/dashboard",
    component: Dashboard,
    page: "dashboard"
  },
  {
    path: "/documents",
    component: Documents,
    page: "documents",
    listPage: true
  },
  {
    path: "/cases",
    component: Cases,
    page: "cases"
  },
  {
    path: "/invoices",
    component: Invoices,
    page: "invoices"
  },
  {
    path: "/tasks",
    component: Tasks,
    page: "tasks"
  },
  {
    path: "/myfiles",
    component: Tasks,
    page: "myfiles"
  },
  {
    path: "/personaldata",
    component: PersonalData,
    page: "personaldata"
  },
  {
    path: "/contact",
    component: Contact,
    page: "contact"
  }
];

const caseRoutes = [
  {
    path: "/case",
    component: Case,
    page: "case"
  }
];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: "",
      designMode: false,
      //TO - DO
      download: false,
      selected: ""
    };
  }
  componentDidMount() {
    this.SwitchDesignMode();
  }
  // shouldComponentUpdate(prevProps) {
  //   const { location } = this.props;
  //   console.log(location);
  //   const locationChanged =
  //     this.props.location !== prevProps.location;
  //     console.log(locationChanged, this.props.location);  
  //   if (locationChanged) {
  //     this.setState({ page: location.state.page});
  //   }
  // }

  SendDetailsViewData = data => {};

  SwitchDesignMode = () => {
    this.state.designMode
      ? (document.designMode = "on")
      : (document.designMode = "off");
  };

  render() {
    const { download } = this.state;
    const { location } = this.props;
    const { Consumer } = PortalContext;
    const { SwitchPageView, chooseCase } = this;
    const URL = process.env.PUBLIC_URL;

    const items = Array.from(Array(100).keys());

    //const page = location.state.page;
    //const page = location.state.page;

    return (
      <ContextProvider>
        {/* <Route exact path="/logout" render={props => ( <EmptyState /> )} /> */}
        <Consumer>
          {context =>

                <UIShell 
                  page={context.page} 
                  SwitchPageView={SwitchPageView} 
                  settings={settings}
                > 
                  <LocationContext context={context} location={location} />
                  <MainContent 
                    page={context.page}
                  >


                    {/* Migrate to ContextAPI */}
                    {download ? <DownloadPanel /> : null}

                    <Switch>
                      {routes.map((route, i) => <Routes key={i} {...route} context={context} />)}
                      {caseRoutes.map((route, i) => <Routes key={i} {...route} context={context} />)}
                      <Route exact path="/" render={() => <Redirect to={{ 
                                                                      pathname: '/dashboard', 
                                                                      state: { page: 'dashboard' }}} />} />
                    </Switch>
                    

                    
                  </MainContent>
                </UIShell>

          }
        </Consumer>

        
      </ContextProvider>
    );
  }
}
export default withRouter(App);

const MainContent = ({ children, page }) => {
  const classes = `cp-MainContent ${page}`;
  return (
    <main className={classes}>
      {children}
    </main>
  );
};


// https://reacttraining.com/react-router/web/example/route-config

const Routes = route => {
  // temporary generator of content
  const items = Array.from(Array(100).keys());

  return (
    <Route
      path={route.path}
      to={{ 
        pathname: route.path, 
        state: { page: route.page }
      }}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component
          page={route.page} 
          items={items}
          listPage={true}

          {...props}
        />
      )}
    />
  );
};


class LocationContext extends Component {
  componentDidMount() {
    //const { location } = this.props;
    //console.log("didMount", location);
  }
  componentDidUpdate(prevProps) {
    const { context, location } = this.props;
    //console.log(location);
    const locationChanged =
      this.props.location !== prevProps.location;
      //console.log(locationChanged, this.props.location);  
    if (locationChanged) {
      context.setActivePage(location.state.page);
    }
  }
  render() {
    return null;
  }
}

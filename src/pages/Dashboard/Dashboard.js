import React from "react";
import { Link } from "react-router-dom";
import { PortalContext } from 'ContextStore';

import PageGenerator from "components/elements/PageGenerator/BasePageGenerator";
import Icon from "components/assets/Icon";
import Persona from "components/assets/Persona/Persona";
import EmptyState from "components/assets/EmptyState"

// import EmptyState, { Empty } from "../components/assets/EmptyState";
import * as Func from "../../utils/Lib";
 // eslint-disable-next-line
import * as Data from "../../utils/Data";

import InfoDetails, { InfoDetailsItem, InfoDetailsItemClickable } from "components/assets/InfoDetails/InfoDetails";

import "pages/pages.scss";
import "./Dashboard.scss";





const userActionLinks = [
  {
    desc: "You have access to:",
    icon: "cases",
    label: "Cases",
    amount: "100",
    path: { 
      pathname: '/cases', 
      state: { page: 'cases' }
    }
  },
  {
    desc: "You have access to:",
    icon: "document",
    label: "Documents",
    amount: "100",
    path: { 
      pathname: '/documents', 
      state: { page: 'documents' }
    }
  },
  {
    desc: "You have access to:",
    icon: "invoice",
    label: "Invoices",
    amount: "100",
    path: { 
      pathname: '/invoices', 
      state: { page: 'invoices' }
    }
  },
  {
    desc: "You have access to:",
    icon: "tasks",
    label: "Tasks",
    amount: "100",
    path: { 
      pathname: '/tasks', 
      state: { page: 'tasks' }
    }
  },
]


const Dashboard = ({ page }) => {
  const { Consumer } = PortalContext;
  return (
    <PageGenerator page={page}>
        <Consumer> 
          {context =>
          <> 
            <Welcome userName={context.user.name} />
            
            {context.client.selectedAccount !== null 
            ? (
              <>
                <UserActions>
                  <ul>
                    {userActionLinks.map((tile, i) =>
                      <li className="TileContainer" key={i}><Tile {...tile} /></li>
                    )}
                  </ul>
                </UserActions>
                <Info selectedAccount={context.client.selectedAccount} />
              </>         
            ) : (
              <EmptyState image="notify" primary
                title="No client account selected"
                subtext=" You haven't selected account to filter portal content. Please select clients' account below or select them from left menu"
              />
            )}
          </>
          }
      </Consumer>
    </PageGenerator>
  );
}

export default Dashboard;

// TO-DO
 // eslint-disable-next-line


const Welcome = ({ userName }) => (
  <>
    <div className="dashboard-greeting-container">
      <h2>{`${Func.GetTimeOfDay()} ${userName}!`}</h2>
    </div>
    <div>
      <h2 className="cp-Heading heading-semibold">Welcome to Portal</h2>
    </div>
  </>
);



const UserActions = ({ children }) => {
  //const
  return (
    <section className="user-actions-section">
      <div>
        <h4 className="heading">What do you want to start with?</h4>
      </div>
      <div className="dashboard-tile-content selection-container">
        {children}
      </div>
    </section>
  )
}
const Tile = props => {
  return (
    <Link to={props.path} className="tile-collection-link" role="link">
      <div className="cp-Tile flex-103">
        <div className="cp-Tile-header flex-100">
          <p className="cp-Tile-title">{props.desc}</p>
        </div>
        <div className="cp-Tile-content flex-104">
          <Icon className="TileIcon" type={props.icon} />
          <span className="TileLabel">{`${props.amount} ${props.label}`}</span>
        </div>
      </div>
    </Link>
  )
};


const Info = ({ selectedAccount }) => (
  <div>
    <section className="selected-client-infodetails">
      <div>
        <h2 className="cp-Heading heading-semibold">Selected client information</h2>
      </div>
     
      <div className="selected-client-infodetails-container">
        <div className="client-details">
          <InfoDetails title="Client's general information">
            <Persona icon="company" label={selectedAccount.name} />
            <InfoDetailsItem data={{ title: "Headquater's address", desc: "undefined" }} />
            <InfoDetailsItem data={{ title: "Tax ID", desc: "N/A" }} />
            <InfoDetailsItem data={{ title: "Aditional ID", desc: "N/A" }} />
          </InfoDetails>
        </div>
        <div className="client-caseManager-details">
          <InfoDetails title="Client's case manager">
            <Persona icon="persona" label="Joe Lawrence" />
            <InfoDetailsItem data={{ title: "Position", desc: "CEO" }} />
            <InfoDetailsItem data={{ title: "Phone", desc: "+01 234 567 890" }} />
            <InfoDetailsItemClickable data={{ title: "E-mail", desc: "j.lawrence@lawrence.com" }} />
          </InfoDetails>
        </div>
      </div>
    </section>
  </div>
);




// const ClientItem = ({ name }) => {
//   return (
//     <div className={`selected-client-item`} title={name}>
//       <div className="cp-Persona">
//         <div className="icon-container">
//           {/* <Icon className="Icon" type="company" /> */}
//           <div className="Initials">
//             <span>{Func.GetInitials(`${name}`)}</span>
//           </div>
//         </div>
//       </div>
//       <div
//         className="selected-client-name"
//         // ariaLabel={`Select company ${name}`}
//       >
//         <span>{name}</span>
//       </div>
//     </div>
//   );
// };
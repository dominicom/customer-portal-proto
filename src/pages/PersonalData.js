import React from "react";
import { PortalContext } from 'ContextStore';

import PageGenerator from "components/elements/PageGenerator/PageGenerator";
// import Icon from "components/assets/Icon";

import Persona from "components/assets/Persona/Persona";
import Card, { CardContainer } from "components/assets/Card/Card"

// import * as Func from "../utils/Lib";
// import * as Data from "../utils/Data";

import InfoDetails, {
  InfoDetailsItem,
  InfoDetailsItemClickable
} from "../components/assets/InfoDetails/InfoDetails";
import "./pages.scss";

const USER_NAME = "Astrid Nielsen";
// const CASE_MANAGER_NAME = "John Lawrence";

const withContext = Wrapped => props => {
  const { Consumer } = PortalContext; 
  return (
    <Consumer>
        {context => <Wrapped {...props} contextValue={context}/>}
    </Consumer>
  );
}

const PersonalData = ({ page }) => (
  <PageGenerator page={page}>
    <div className="PageHeader header-container">
      <Persona label={USER_NAME} icon="persona" kind="medium" />
    </div>
    <CardContainer>
      <section className="personal-data-section">
        <div>
          <h2 className="heading-semibold">Personal data</h2>
        </div>

        <Card>
          <InfoDetails>
            <InfoDetailsItemClickable
              data={{ title: "E-mail", desc: "a.lawrence@gmail.com" }}
              alt="Go to office website"
              tooltip="Copy office website adress"
            />
            <InfoDetailsItemClickable
              data={{ title: "Website", desc: "www.lawandrenceoffice.com" }}
              alt="Go to office website"
              tooltip="Copy office website adress"
            />
            <InfoDetailsItem
              data={{ title: "Address", desc: "Unforgetable Street" }}
            />
            <InfoDetailsItem data={{ title: "Postal code", desc: "01-23" }} />
            <InfoDetailsItem data={{ title: "City", desc: "Thistown City" }} />
          </InfoDetails>
        </Card>
      </section>

      <section className="additional-consents-section">
        <div>
          <h2 className="heading-semibold">Additional consents</h2>
        </div>

        <Card>
          <InfoDetails>

            <InfoDetailsItem
              data={{ title: "Consent to use in other contexts", desc: "Yes" }}
            />

            <InfoDetailsItem
              data={{ title: "Invitation to events", desc: "Yes" }}
            />
            <InfoDetailsItem
              data={{ title: "Legal alerts", desc: "Yes" }}
            />
            <InfoDetailsItem
              data={{ title: "Newsletter", desc: "Yes" }}
            />
          </InfoDetails>
        </Card>
      </section>
    </CardContainer>
  </PageGenerator>
);

export default withContext(PersonalData);

// const Tile = () => {
//   return <div className="cp-Tile" />;
// };

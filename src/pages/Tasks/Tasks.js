import React, { Component } from "react";
import { PortalContext } from 'ContextStore';
import PageGenerator from "components/elements/PageGenerator/BasePageGenerator";

import ActionButton from "components/assets/Buttons/ActionButton";
import Filter from "components/assets/Filter/Filter";
import Icon from "components/assets/Icon";

import Table, {
  Header,
  Body,
  Row,
  Cell,
  ItemName,
  TileField
} from "components/assets/Table/Table";

//import EmptyState from "components/assets/EmptyState";

const ItemsGenerator = Array.from(Array(100).keys());


const tasksData = {
  header: [
    { 
      data: "Subject",
      type: "title"
    }, 
    {
      data: "Case",
      type: "caseType"
    }, 
    { 
      data: "Realization date",
      type: "date"
    }, 

    { 
      data: "Assigned to",
      type: "date"
    }
  ],
  row: [
    {  
      data: "Divorce case",
      type: "caseType"
    },
    {  
      data: "20/05/04",
      type: "date"
    },
    {
      data: "Astrid Nielsen",
      type: "date"
    }
  ]
}

const isOdd = (num) => { 
  const result = num % 2;
  if (result == 0) {
    return true;
  } else {
    return false;
  }
}


const TitleItem = (i, type, link ) => {
  return {
    data: <>
            <ItemName
              title={isOdd(i) ? "Meeting" : "Signing documents"}
              mainAction={{ icon: "open" }}
            />
          </>,
    type: type
  };
}
// const CaseId = (i, type ) => {
// return {
//     data: `00/0${i}/6743/345`,
//     type: "caseId"
//   };
// }

const TileTitleItem = (i, type) => {

  return {
    data: <>
            <ItemName
                title={isOdd(i) ? "Meeting" : "Signing documents"}
                //  link={link}
            />
          </>,
    type: type
  };
}
export default class Tasks extends Component {
    constructor(props) {
    super(props);
    this.state = {
      selectedCase: "",
      selectedCaseId: "-none-",
      items: []
    };
  }
  componentDidMount() {
    const items = ItemsGenerator.map(x => `Case name #${x + 1}`);
    //this.setState({ items: items }, () => console.log(this.state.items));
    this.setState({ items: items });
  }
  ChooseTask = (title, id) => {
    console.log(title);
    this.setState({ selectedCase: title, selectedCaseId: `#${id}` });
  };
  MainView = () => {
    console.log("mainview");
    this.setState({ selectedCase: "" });
  };

  render() {
    const { Consumer } = PortalContext;
    const { page, SwitchPageView } = this.props;
    const { ChooseTask, MainView } = this;
    const { selectedCase, selectedCaseId, items } = this.state;
    return (

      <PageGenerator page={page} listPage>
        <Consumer> 
          {context => 
            <>
            {!context.mobile ? (
              <Table>
                <Header 
                  iconPlaceholder
                >
                  
                      {tasksData.header.map((el, i) => (
                        <Cell key={i} type={el.type}>{el.data}</Cell>
                      ))}
                    
                </Header>
                <Body>
                  {items.map((row, i) => (
                    <Row
                      key={i}
                      icon={isOdd(i) ? "event" : "task"}
                      link={() => {
                        ChooseTask(row, i + 1); 
                      }}
                    >
                      <>
                        {[ TitleItem(i, "title"), ...tasksData.row].map((el, i) => (
                          <Cell key={i} type={el.type}>{el.data}</Cell>
                        ))}
                      </>
                    </Row>
                  ))}
                </Body>
              </Table>
            ) : (
              <Table>
                <Header 
                  iconPlaceholder
                  tile
                >
                  <div className="HeaderTitle">Subject</div>
                  <Filter label="A to Z" />
                </Header>
                <Body>
                  {items.map((row, i) => (
                    <Row tile
                      key={i}
                      icon={isOdd(i) ? "event" : "task"}
                      link={() => {
                        ChooseTask(row, i + 1); 
                      }}
                    >
                        <TileField>
                          {[ TileTitleItem(i, "title"), ...tasksData.row].map((el, i) => (
                            <Cell key={i} type={el.type}>{el.data}</Cell>
                            // <div className="" key={i}>{el.data}</div>
                          ))}
                        </TileField>

                        <div className="main-action">
                          <ActionButton icon="open" />
                        </div>
                    </Row>
                  ))}
                </Body>
              </Table>
            )}
            </>
          }
        </Consumer>
      </PageGenerator>
        
    );
  }
}




{/* <EmptyState image="notify" primary
title="Sorry, Tasks section is under construction"
subtext="We apologize but Tasks has still fixing table constructor"
/> */}
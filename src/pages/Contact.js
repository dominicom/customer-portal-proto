import React from "react";
import { PortalContext } from 'ContextStore';

import PageGenerator from "components/elements/PageGenerator/PageGenerator";


import Card, { CardContainer } from "components/assets/Card/Card"
import Persona from "components/assets/Persona/Persona";
import InfoDetails, {
  InfoDetailsItem,
  InfoDetailsItemClickable
} from "components/assets/InfoDetails/InfoDetails";

import "./pages.scss";


const withContext = Wrapped => props => {
  const { Consumer } = PortalContext; 
  return (
    <Consumer>
        {context => <Wrapped {...props} contextValue={context}/>}
    </Consumer>
  );
}

const Contact = ({ page, contextValue }) => {
  const context = contextValue;
  return (
    <PageGenerator page={page}>
      <div className="PageHeader header-container">
        <Persona label={context.organisation.name} icon="organisation" kind="medium" />
      </div>

      <CardContainer>
        <section className="general-contact-section">
          <div>
            <h2 className="heading-semibold">General contact</h2>
          </div>
          {/* <div className="general-contact-container flex-b2r"> */}
          
          <Card>
            <InfoDetails>
              {/* "InfoDetailsItem" is equvalent of Ng "info-pane" element */}
              <InfoDetailsItemClickable
                data={{ title: "E-mail", desc: "a.lawrence@gmail.com" }}
                alt="Go to office website"
                tooltip="Copy office website adress"
              />
              <InfoDetailsItemClickable
                data={{ title: "Website", desc: "www.lawandrenceoffice.com" }}
                alt="Go to office website"
                tooltip="Copy office website adress"
              />
              <InfoDetailsItem
                data={{ title: "Address", desc: "Unforgetable Street" }}
              />
              <InfoDetailsItem data={{ title: "Postal code", desc: "01-23" }} />
              <InfoDetailsItem data={{ title: "City", desc: "Thistown City" }} />
            </InfoDetails>
          </Card>
        </section>

        <section className="dedicated-contact-section">
          <div>
            <h2 className="heading-semibold">Dedicated contact</h2>
          </div>
          {/* <div className="dedicated-contact-container "> */}
          <Card>
            <InfoDetails>
              <Persona label="John Lawrence" icon="persona" />
              {/* "InfoDetailsItem" is equvalent of Ng "info-pane" element */}
              <InfoDetailsItem
                data={{
                  title: "Position",
                  desc: "CEO & Co-founder, Lawyer"
                }}
              />
              <InfoDetailsItem data={{ title: "Phone", desc: "+01 789 654 321" }} />
              <InfoDetailsItemClickable
                data={{ title: "E-mail", desc: "j.lawrence@gmail.com" }}
                alt="Go to office website"
                tooltip="Copy office website adress"
              />
            </InfoDetails>
          </Card>
        </section>
      </CardContainer>
    </PageGenerator>
  );
}

export default withContext(Contact);

const Tile = () => {
  return <div className="cp-Tile" />;
};

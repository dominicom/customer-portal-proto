import React from "react";
import { PortalContext } from 'ContextStore';

import PageGenerator from "components/elements/PageGenerator/BasePageGenerator";

import ActionButton from "components/assets/Buttons/ActionButton";
import Filter from "components/assets/Filter/Filter";
import * as data from "utils/Data";



import Table, {
  Header,
  Body,
  Row,
  Cell,
  ItemName,
  TileField
} from "../../components/assets/Table/Table";
import Icon from "../../components/assets/Icon";


const invoicesData = {
  header: [
    { 
      data: "No.",
      type: "title"
    }, 
    {
      data: "Case",
      type: "caseName"
    }, 
    {
      data: "Gross value",
      type: "grossValue"
    }, 
    { 
      data: "Currency",
      type: "currency"
    }, 
    { 
      data: "Due date",
      type: "date"
    }
  ],
  row: [
    { 
      data: "Lorem Ipsum",
      type: "caseName" 
    },
    {  
      data: `99.00`,
      type: "grossValue"
    },
    {
      data: "Euro",
      type: "currency"

    }, 
    {
      data: "20/02/2020",
      type: "date"
    }
  ]
}


const TitleItem = (i, type) => {
 return {
   data: <ItemName
          title={`INV/${Math.floor(Math.random() * 100)}/${i + 1}/20.pdf`}
          link={e => {
            e.stopPropagation();
            // handleClickItem(e.target.title, i + 1);
          }}
          mainAction={{ icon: "download" }}
        />,
    type: type
  };
}
const TileTitleItem = (i, type) => {
 return {
   data: <ItemName
          title={`INV/${Math.floor(Math.random() * 100)}/${i + 1}/20.pdf`}
          link={e => {
            e.stopPropagation();
            // handleClickItem(e.target.title, i + 1);
          }}
        />,
    type: type
  };
}


const Invoices = ({ page, items }) => {
  const { Consumer } = PortalContext;
  return (
    
    <PageGenerator page={page} listPage>
      <Summary />
      <Consumer> 
        {context => 
          <>
          {!context.mobile ? (
            <Table detailed>
              <Header icon="file">
      
                    {invoicesData.header.map((el, i) => (
                      <Cell key={i} type={el.type}>{el.data}</Cell>
                    ))}
      
              </Header>
              <Body>
                {items.map(i => (
                  <Row
                    key={i}
                    icon="pdf"
                  >
      
                      {[ TitleItem(i, "title",), ...invoicesData.row].map((el, i) => (
                        <Cell key={i} type={el.type}>{el.data}</Cell>
                      ))}
        
                  </Row>
                ))}
              </Body>
            </Table>
          ) : (
            <Table>
              <Header icon="file" tile>
                <div className="HeaderTitle">Invoice</div>
                <Filter label="A to Z" />
              </Header>
              <Body>
                {items.map(i => (
                  <Row tile
                    key={i}
                    icon="pdf"
                  >
                      <TileField>
                        {[ TileTitleItem(i, "title"), ...invoicesData.row].map((el, i) => (
                          <Cell key={i} type={el.type}>{el.data}</Cell>
                          // <div className="" key={i}>{el.data}</div>
                        ))}
                      </TileField>

                      <div className="main-action">
                        <ActionButton icon="download" />
                      </div>
                  </Row>
                ))}
              </Body>
            </Table>
          )}
        </>
      }
    </Consumer>
    </PageGenerator>
  );
}

export default Invoices;

const Summary = () => {
  return (
    <div className="cp-Summary">
      <SummaryItem title="Incoming payments" type="due">
        {data.balance.due.map((row, i) => (
          <SummaryRow key={i} {...row} />
        ))}
      </SummaryItem>
      <SummaryItem title="Overdue payments" type="overdue">
        {data.balance.overdue.map((row, i) => (
          <SummaryRow key={i} {...row} />
        ))}
      </SummaryItem>
    </div>
  );
};
const SummaryItem = ({ children, title, type }) => {
  // eslint-disable-next-line
  const Warning = (
    <div className="overdue-warning">
      <Icon className="Icon" type="info" />
      <span> 4 day(s) </span>
    </div>
  );
  const Title = (
    <div className="cp-SummaryItem-title">
      <h3 className="display-name">{title}</h3>
      {/* {type === "overdue" ? Warning : null} */}
    </div>
  );

  return (
    <Tile type={type}>
      {Title}
      <div className="cp-SummaryItem-content">{children}</div>
    </Tile>
  );
};

const Tile = ({ children, type }) => {
  return (
    <div className={`cp-SummaryItem${type ? ` ${type}` : ""}`}>{children}</div>
  );
};

const SummaryRow = data => {
  return (
    <div className="SummaryRow">
      <Coin currency={data.currency} />
      <div className="invoices flex-101">
        <Icon className="Icon" type="invoice" />
        <span>{data.invoices}</span>
      </div>
      <div className="ballance">
        <span>{data.summary}</span>
        <span>{data.currency}</span>
      </div>
    </div>
  );
};

const Coin = ({ currency }) => {
  return (
    <div className="Coin">
      <div className="Coin-label">{currency}</div>
    </div>
  );
};

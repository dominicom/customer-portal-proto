import React, { Component } from "react";

import { PortalContext } from 'ContextStore';

import PageGenerator from "components/elements/PageGenerator/BasePageGenerator";

import ActionButton from "components/assets/Buttons/ActionButton";
import Filter from "components/assets/Filter/Filter";
import Icon from "components/assets/Icon";

import Table, {
  Header,
  Body,
  Row,
  Cell,
  ItemName,
  TileField
} from "components/assets/Table/Table";

// import Case from "pages/Cases/subpages/Case";

const ItemsGenerator = Array.from(Array(100).keys());


const casesData = {
  header: [
    { 
      data: "Name",
      type: "title"
    }, 
    { 
      data: "Case ID",
      type: "caseId"
    }, 
    {
      data: "Case type",
      type: "caseType"
    }, 
    { 
      data: "Status",
      type: "date"
    }
  ],
  row: [
    {  
      data: "Divorce case",
      type: "caseType"
    },
    {
      data: "In progress",
      type: "date"
    }
  ]
}
const Relations = () => (
  <div className="relations">
    <span><Icon className="relIcon" type="document"/> 6</span>
    <span><Icon className="relIcon" type="invoice"/> 5</span>
    <span><Icon className="relIcon" type="tasks"/> 4</span>
  </div>
)

const TitleItem = (i, type, link ) => {
  return {
    data: <>
            <ItemName
              title={`Case name #${i + 1}`}
              mainAction={{ icon: "open" }}
            />
            <Relations />
          </>,
    type: type
  };
}
const CaseId = (i, type ) => {
return {
    data: `00/0${i}/6743/345`,
    type: "caseId"
  };
}

const TileTitleItem = (i, type) => {
  return {
    data: <>
            <ItemName
                title={`Case name #${i + 1}`}
                //  link={link}
            />
            <Relations />
          </>,
    type: type
  };
}

export default class Cases extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedCase: "",
      selectedCaseId: "-none-",
      items: []
    };
  }
  componentDidMount() {
    const items = ItemsGenerator.map(x => `Case name #${x + 1}`);
    //this.setState({ items: items }, () => console.log(this.state.items));
    this.setState({ items: items });
  }
  ChooseCase = (title, id) => {
    console.log(title);
    this.setState({ selectedCase: title, selectedCaseId: `#${id}` });
  };
  MainView = () => {
    console.log("mainview");
    this.setState({ selectedCase: "" });
  };

  render() {
    const { Consumer } = PortalContext;
    const { page, SwitchPageView } = this.props;
    const { ChooseCase, MainView } = this;
    const { selectedCase, selectedCaseId, items } = this.state;
    return (

      <PageGenerator page={page} listPage>
        <Consumer> 
          {context => 
            <>
            {!context.mobile ? (
              <Table>
                <Header>
                  
                      {casesData.header.map((el, i) => (
                        <Cell key={i} type={el.type}>{el.data}</Cell>
                      ))}
                    
                </Header>
                <Body>
                  {items.map((row, i) => (
                    <Row
                      key={i}
                      //icon="cases"
                      link={() => {
                        ChooseCase(row, i + 1); 
                      }}
                    >
                      <>
                        {[ TitleItem(i, "title"), CaseId(i), ...casesData.row].map((el, i) => (
                          <Cell key={i} type={el.type}>{el.data}</Cell>
                        ))}
                      </>
                    </Row>
                  ))}
                </Body>
              </Table>
            ) : (
              <Table>
                <Header 
                  //icon="cases" 
                  tile
                >
                  <div className="HeaderTitle">Case</div>
                  <Filter label="A to Z" />
                </Header>
                <Body>
                  {items.map((row, i) => (
                    <Row tile
                      key={i}
                      //icon="cases"
                      link={() => {
                        ChooseCase(row, i + 1); 
                      }}
                    >
                        <TileField>
                          {[ TileTitleItem(i, "title"), ...casesData.row].map((el, i) => (
                            <Cell key={i} type={el.type}>{el.data}</Cell>
                            // <div className="" key={i}>{el.data}</div>
                          ))}
                        </TileField>

                        <div className="main-action">
                          <ActionButton icon="open" />
                        </div>
                    </Row>
                  ))}
                </Body>
              </Table>
            )}
            </>
          }
        </Consumer>
      </PageGenerator>
        
    );
  }
}

// const CaseName = ({ name, onClick }) => (
//   <a onClick={onClick} title={name}>
//     {name}
//   </a>
// );

{
  /* <CaseName
name={`Case name #${i + 1}`}
onClick={e => ChooseCase(e.target.title, i + 1)}
/>, */
}

// onClick={e => ChooseCase(e.target.title, i + 1)}

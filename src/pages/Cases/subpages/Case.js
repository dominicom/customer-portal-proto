import React from "react";


import InfoDetails, {
  InfoDetailsItem,
  InfoDetailsItemClickable
} from "components/assets/InfoDetails/InfoDetails";

import SubPageGenerator from "components/elements/PageGenerator/SubPageGenerator";
import TabNavigator from "components/assets/TabNavigator/Pivot";

// import Table, {
//   Header,
//   Body,
//   Row,
//   Cell
// } from "../../components/assets/Table/Table";
// import Icon from "../../components/assets/Icon";

import "./Case.scss";

const Case = ({ page, crumbs, title, SwitchPageView, id }) => (
  <SubPageGenerator
    page={page}
    title={title}
    crumbs={crumbs}
    SwitchPageView={SwitchPageView}
  >
    <div className="cp-Pagetabs">
      <TabNavigator />
    </div>

    <div className="cp-PageContent">
      {/* TO-DO */}
      <section className="cp-View-CaseDetails">
        {/* <article
          style={{
            display: `flex`,
            flexDirection: `column`,
            marginRight: `2rem`
          }}
        >
          <InfoPane title="Details">
            <InfoData data={{ title: "File Type", desc: "Microsoft Word" }} />
            <InfoData data={{ title: "Size", desc: "456kb" }} />
          </InfoPane>
        </article> */}
        <article
          style={{
            display: `flex`,
            flexDirection: `column`
          }}
        >
          <InfoDetails title="Case information">
            <InfoDetailsItem
              data={{ title: "ID", desc: id ? id : "undefined" }}
            />
            <InfoDetailsItem
              data={{ title: "Case type", desc: "Divorce case" }}
            />
            <InfoDetailsItem data={{ title: "Status", desc: "In progress" }} />
            <InfoDetailsItem
              data={{ title: "Client", desc: "Astrid Nielsen" }}
            />
            <InfoDetailsItem
              data={{ title: "Settlement type", desc: "Type of settlement" }}
            />
          </InfoDetails>
        </article>
        <article
          style={{
            display: `flex`,
            flexDirection: `column`
          }}
        >
          <InfoDetails title="Case manager">
            <InfoDetailsItem
              data={{ title: "Person", desc: "John Lawrence" }}
            />
            <InfoDetailsItem
              data={{ title: "Phone", desc: "+00 123 456 789" }}
            />
            <InfoDetailsItemClickable
              data={{ title: "E-mail", desc: "a.lawrence@gmail.com" }}
              alt="Go to office website"
              tooltip="Copy office website adress"
            />
            <InfoDetailsItemClickable
              data={{ title: "Website", desc: "www.lawandrenceoffice.com" }}
              alt="Go to office website"
              tooltip="Copy office website adress"
            />
          </InfoDetails>
        </article>
        {/* <article
          style={{
            display: `flex`,
            flexDirection: `column`
          }}
        >
          <InfoDetails title="Another details pane (only for testing) with looooong name">
            <InfoDetailsItem
              data={{
                title: "Person",
                desc: "Sir. Joe Edward Bernard 'Jeb' Lawrence (II) Jr. "
              }}
            />
            <InfoDetailsItem
              data={{ title: "Phone", desc: "+01 789 654 321" }}
            />
            <InfoDetailsItemClickable
              data={{ title: "E-mail", desc: "j.lawrence@gmail.com" }}
              alt="Go to office website"
              tooltip="Copy office website adress"
            />
            <InfoDetailsItemClickable
              data={{ title: "Website", desc: "www.lawandrenceoffice.com" }}
              alt="Go to office website"
              tooltip="Copy office website adress"
            />
          </InfoDetails>
        </article> */}
      </section>
    </div>
  </SubPageGenerator>
);

export default Case;

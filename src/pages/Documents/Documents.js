import React, { Component } from "react";
import { PortalContext } from 'ContextStore';

import PageGenerator from "components/elements/PageGenerator/BasePageGenerator";

import ActionButton from "components/assets/Buttons/ActionButton";
import Filter from "components/assets/Filter/Filter";

//import HTMLcontainer from "../HTMLcontainer";
import Table, {
  Header,
  Body,
  Row,
  Cell,
  ItemName,
  TileField
} from "../../components/assets/Table/Table";

//import Icon from "components/assets/Icon";

const documentsData = {
  header: [
    { 
      data: "Name",
      type: "title"
    }, 
    { 
      data: "ID",
      type: "identityNo"
    }, 
    {
      data: "Case",
      type: "caseName"
    }, 
    { 
      data: "Uploaded",
      type: "date"
    }, 
    { 
      data: "Last modified",
      type: "date"
    }
  ],
  row: [
    { 
      data: "Lorem Ipsum",
      type: "caseName" 
    },
    {  
      data: "02/01/2020",
      type: "date"
    },
    {
      data: "20/02/2020",
      type: "date"
    }
  ]
}

const TitleItem = (i, type ) => {
  return {
    data: <ItemName
            title={`Sample-file-name-${i + 1}.ext`}
            link={e => {
              e.stopPropagation();
              // handleClickItem(e.target.title, i + 1);
            }}
            mainAction={{ icon: "download" }}
          />,
    type: type
  };
}
const FileId = (i, type ) => {
 return {
    data: `0${i}/6743`,
    type: "identityNo"
  };
}

const TileTitleItem = (i, type) => {
  return {
    data: <ItemName
           title={`INV/${Math.floor(Math.random() * 100)}/${i + 1}/20.pdf`}
           link={e => {
             e.stopPropagation();
             // handleClickItem(e.target.title, i + 1);
           }}
         />,
     type: type
   };
 }



class Documents extends Component {
  handleClickItem = (title, id) => {
    console.log(title, id);
    //this.setState({ selectedCase: title, selectedCaseId: `#${id}` });
  };
  handleClickRow = (title, id) => {
    console.log("Row clicked!", title);
  };
  render() {
    const { page, items } = this.props;
    const { handleClickItem, handleClickRow } = this;
    const { Consumer } = PortalContext;
    return (
      <PageGenerator page={page} listPage>
      <Consumer> 
        {context => 
          <>
            {!context.mobile ? (
              <Table detailed>
                <Header icon="file">
                      {documentsData.header.map((el, i) => (
                        <Cell key={i} type={el.type}>{el.data}</Cell>
                      ))}
                </Header>

                <Body>
                  {items.map(i => (
                    <Row
                      key={i}
                      icon="textFile"
                      onClick={e => {
                        e.stopPropagation();
                        handleClickRow(i + 1); //
                      }}
                    >
                      <>
                        {[ TitleItem(i, "title"), FileId(i), ...documentsData.row
                        ].map((el, i) => (
                          <Cell key={i} type={el.type}>{el.data}</Cell>
                        ))}
                      </>
                    </Row>
                  ))}
                </Body>
              </Table>
            ) : (
              <Table>
                <Header icon="file" tile>
                  <div className="HeaderTitle">Document</div>
                  <Filter label="A to Z" />
                </Header>
                <Body>
                  {items.map(i => (
                    <Row tile
                      key={i}
                      icon="textFile"
                    >
                        <TileField>
                          {[ TileTitleItem(i, "title"), FileId(i), ...documentsData.row].map((el, i) => (
                            <Cell key={i} type={el.type}>{el.data}</Cell>

                          ))}
                        </TileField>

                        <div className="main-action">
                          <ActionButton icon="download" />
                        </div>
                    </Row>
                  ))}
                </Body>
              </Table>
            )}
          </>
        }
      </Consumer>
      </PageGenerator>
    );
  }
}

export default Documents;

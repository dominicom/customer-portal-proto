import React, { Component } from 'react';

import * as Data from "./utils/Data";

export const PortalContext = React.createContext("");

const BREAK_POINT = 768;
const MENU_COMPACT_MODE = 900;
const MIN_HEIGHT = 450;


export class ContextProvider extends Component {
  state = {
    //navigation: ["work", "about", "contact"],
    page: "dashboard",
    mobile: false,
    compactMode: {
      menu: false,
      commandBar: false
    },
    user: Data.user,
    client: {
      accounts: Data.accounts,
      selectedAccount: null
    },
    organisation: {
      name: "Law & Rence Advokat Firm"
    },
    data: null,
    taskPane: {
      open: false,
      panels: [
        { type: "UserPanel", label: "Profile", icon: "user" },
        { type: "SettingsPanel", label: "Settings", icon: "gear" },
      ],
      active: "UserPanel"
    },
    balance: Data.balance,
    isModalActive: false,
    modalComponent: null,
    isTaskPaneActive: false,
    isUserPanelOpen: false,
    isModalOpen: false,
    isDetailsViewOpen: false,
    designMode: false
  }
  componentDidMount() {
    this.SwitchDesignMode();
    this.SetDefaultAccount(this.state.client.accounts[0])

    this.WindowSize();
    window.addEventListener("resize", this.WindowSize);
  };


  componentWillUnmount() {
    window.removeEventListener("resize", this.WindowSize);
  }


  SwitchDesignMode = () => {
    this.state.designMode
      ? (document.designMode = "on")
      : (document.designMode = "off");
    this.setState(prevState => ({
      designMode: !prevState.designMode
    }));
  };
  OpenModal = (component) => {
    this.setState({ isModalOpen: true });
    this.setState({ modalComponent: component ? { title: component.title, content: component.content } : null });
  };
  SelectClientAccount = account => {
    this.setState(prevState => (
      { client: { ...prevState, selectedAccount: account }}
    ), () => console.info("context -> selected client:", this.state.client.selectedAccount));
  };
  SetDefaultAccount = account => {
    this.setState(prevState => (
      { client: { ...prevState, selectedAccount: account }}
    ), () => console.info("set default client:", this.state.client.selectedAccount));
  };

  WindowSize = () => {
    let width = window.innerWidth;
    //let height = window.innerHeight;

    //const { mobile } = this.state;

    console.log("KONTEKST: lisen to de mobajl!")

    // @PARAM BREAK_POINT     - number, minimum resolution width when menu is for mobile view (contracted)
    // @see top of file
    width < BREAK_POINT
      ? this.setState({ mobile: true  })
      : this.setState({ mobile: false });

    width < MENU_COMPACT_MODE
      ? this.setState(prevState => ({ compactMode: { ...prevState, menu: true  } }))
      : this.setState(prevState => ({ compactMode: { ...prevState, menu: false } }));
  };



  render () {
    const { Provider } = PortalContext;
    const { children } = this.props
    const { SwitchDesignMode, OpenModal, SelectClientAccount } = this
    const { user, page, mobile, client, organisation, compactMode, modalComponent, isTaskPaneActive, isUserPanelOpen, isModalOpen, isDetailsViewOpen } = this.state;
    return (
      <Provider 
        value={{
          user: user,
          page: page,
          mobile: mobile,
          compactMode: compactMode,
          client: client,
          organisation: organisation,
          isTaskPaneActive: isTaskPaneActive,
          isModalOpen: isModalOpen,
          modalComponent: modalComponent,
          isDetailsViewOpen: isDetailsViewOpen,
          setActivePage: page => {
            this.setState({ page: page }, () => console.log("Page:", page ))
          },
          SelectClientAccount: SelectClientAccount,
          // Temp solution
          openTaskPane: (panel, active) => {
            this.setState(prevState => ({
              isTaskPaneActive: !prevState.isTaskPaneActive
            }));
          },

          closeTaskPane: () => {
            this.setState({ isTaskPaneActive: false }, () => console.log("Clicked to close TP"));
          },

          openUserPanel: () => {
            this.setState(prevState => ({
              isUserPanelOpen: !prevState.isUserPanelOpen
            }));
            console.log("user panel open?", isUserPanelOpen)
          },
          openModal: component => OpenModal(component),
          closeModal: () => this.setState({ isModalOpen: false }),

          

          openDetailsPanel: () => {
            this.setState(prevState => ({
              isDetailsViewOpen: !prevState.isDetailsViewOpen
            }));
          },
          openDetails: () => this.setState({ isDetailsViewOpen: true }),
          closeDetails: () => this.setState({ isDetailsViewOpen: false }),
          SwitchDesignMode: SwitchDesignMode,
          toggleMobileMode: () => {
            this.setState(prevState => ({
              mobile: !prevState.mobile
            }));
          },
        }}>
          {children}
      </Provider>
    )
  }
}
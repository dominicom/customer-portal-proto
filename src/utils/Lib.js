import React from "react";

// make tidy strings of classes array, 
// @method join()                   - adds " " between argguments in array of classes
// @method trimLeft() & trimRight() - similary to trim() reduces extra spaces at begining and end of string
// @method replace(/\s\s+/g, " ")   - reduces extra double spaces in result string

export const tidy = (classes) => {
  return classes.join(" ").trimLeft().trimRight().replace(/\s\s+/g, " ");
}

// Delay unMounting Component utility

//export const delayUnmounting = Delayed => {
export function delayUnmounting(Delayed) {
  return class extends React.Component {
    state = {
      shouldRender: this.props.isMounted
    };
    componentDidUpdate(prevProps) {
      if (prevProps.isMounted && !this.props.isMounted) {
        setTimeout(
          () => this.setState({ shouldRender: false }),
          this.props.delayTime
        );
      } else if (!prevProps.isMounted && this.props.isMounted) {
        this.setState({ shouldRender: true });
      }
    }
    render() {
      return this.state.shouldRender ? <Delayed {...this.props} /> : null;
    }
  };
}


// CLIENTS' PERSONA functions

export const GetInitials = name => {
  var initials = name.match(/\b\w/g) || [];
  initials = ((initials.shift() || "") + (initials.pop() || "")).toUpperCase();
  return initials;
};

const colorHashCode = str => {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
};

const colorFromHexToRgb = i => {
  let c = (i & 0x00ffffff).toString(16);

  return "00000".substring(0, 6 - c.length) + c;
};

export const GetColor = name => {
  return "#"+colorFromHexToRgb(colorHashCode(name));
};


export const GetTimeOfDay = () => {
  let hour = new Date().getHours();
  let text;
  if (hour > 18) {
    text = "Good evening";
  } else if (hour > 12) {
    text = "Good afternoon";
  } else {
    text = "Good morning";
  }
  return text;
};

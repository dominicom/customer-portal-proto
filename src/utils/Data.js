



export const user = {
  name: "Astrid",
  lastname: "Nielsen",
  mail: "astrid.nielsen@gmail.com"
};


export const accounts = [
    { name: "Fast Company", id: "01162" },
    { name: "ACME Corporation", id: "02823" },
    { name: "Massive Dynamics", id: "03824" },
    { name: "Weyland Yutani Inc.", id: "04034" },
    { name: "Initech", id: "05643" },
    { name: "Globex Corp.", id: "06023" },
    { name: "Vehement Capital Partners", id: "07193" },
    { name: "Soylent Corp.", id: "08324" },
    { name: "Farrell LLC", id: "09428" },
    { name: "Zboncak, Rohan and Carroll Group", id: "10742" }
  ];


export const USER_NAME = user.name;


export const balance = {
  due: [
    {
      currency: "USD",
      invoices: 6,
      summary: "128.00"
    },
    {
      currency: "EUR",
      invoices: 4,
      summary: "256.00"
    },
    {
      currency: "PLN",
      invoices: 3,
      summary: "512.00"
    }
  ],
  overdue: [
    {
      currency: "USD",
      invoices: 6,
      summary: "128.00"
    },
    {
      currency: "EUR",
      invoices: 4,
      summary: "256.00"
    },
    {
      currency: "PLN",
      invoices: 3,
      summary: "512.00"
    },
    {
      currency: "CZK",
      invoices: 5,
      summary: "1 024 000.00"
    }
  ]
}

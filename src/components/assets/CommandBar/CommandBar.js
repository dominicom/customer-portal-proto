import React, { Component } from "react";
import { PortalContext } from 'ContextStore';

import UploadFile from "components/elements/UploadFile/UploadFile";

import ActionButton from "components/assets/Buttons/ActionButton";
import Search from "components/assets/Search/Search";

import "./CommandBar.scss";

export default class CommandBar extends Component {
  render() {
    const { Consumer } = PortalContext;
    return (
      <Consumer> 
        {context => 
          <div className="cp-CommandBar commandBar">
            <Actions kind="primary">
              <ActionButton
                kind="primary"
                icon="upload"
                label={!context.mobile ? "Upload document" : null}
                onClick={() => context.openModal({ title: "Upload file", content: <UploadFile /> })}
              />
            </Actions>
            <Actions kind="secondary">

              <ActionButton
                kind="primary"
                icon="info"
                label={!context.mobile ? "Details" : null}
                onClick={() => context.openDetailsPanel()}
              />

              <Search />
            </Actions>
          </div>
        }
      </Consumer>
    );
  }
}

const Actions = ({ kind, children }) => (
  <div className={`${kind}-actions`}>{children}</div>
);

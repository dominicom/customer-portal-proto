import React from "react";

import "./Footer.css";

const Footer = props => {
  return (
    <footer>
      <div className="footer-content">
        <div className="footer-content-left">
          Copyright © 2019 Advisor. All Rights Reserved.
        </div>
        <div className="footer-content-right">
          <strong>Advisor 247 Portal</strong> is delivered by{" "}
          <strong>Advisor AS</strong>, supplier of Case Management System{" "}
          <strong>Advisor247 Legal</strong>  and CRM system -{" "}
          <strong>Advisor247 CRM</strong>.
        </div>
      </div>
    </footer>
  );
};

export default Footer;

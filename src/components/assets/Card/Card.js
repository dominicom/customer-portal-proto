import React from "react";

// import Icon from "components/assets/Icon";

import { tidy } from "utils/Lib";

import "./Card.scss";


export default function Card({
  className,
  children
}) {


  const classes = ["cp-Card", className];
  return (
    <div className={tidy(classes)}>
      {children}
    </div>
  );
}

export const CardContainer = ({ children }) => (
  <div className="card-container">{children}</div>
)
import React, { Component } from "react";

import "./Pivot.scss";

export default class Pivot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabs: ["Details", "Documents", "Invoices", "Tasks"],
      selected: "Details"
    };
  }
  render() {
    const { tabs, selected } = this.state;
    return (
      <div className="cp-TabNavigator ms-Pivot">
        <ul className="cp-Tab-links ms-Pivot-links">
          <>
            {tabs.map((tab, i) => (
              <PivotItem key={i} title={tab} selected={selected} />
            ))}
          </>
        </ul>
      </div>
    );
  }
}

const PivotItem = ({ title, selected }) => {
  const active = title === selected ? " is-selected" : "";
  return <li className={`cp-Tab-link tab ms-Pivot-link${active}`}>{title}</li>;
};

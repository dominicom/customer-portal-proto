import React from "react";

import Icon from "./Icon";
import Spinner from "components/assets/Spinner/Spinner";

const Panel = ({ children }) => <div className="DownloadPanel">{children}</div>;
const FileItem = () => (
  <div className="DownloadPanel-item">
    <div className="DownloadPanel-itemContainer">
      <Spinner small />
      <div className="DownloadPanel-itemLabel">
        Please wait for download <span>File Name.ext</span>
      </div>
    </div>
  </div>
);

export default ({ type, image, ...other }) => {
  return (
    <Panel>
      <FileItem />
      <FileItem />
      <FileItem />
    </Panel>
  );
};

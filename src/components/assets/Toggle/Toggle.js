import React, { useState } from "react";

import { tidy } from "utils/Lib";

import "./Toggle.scss";



export default function Button({
  onChange,
  label,
  disabled,
  checked,
  onText,
  offText
}) {
  const [on, setOn] = useState(checked ? true : false);

  const Switch = () => {
    setOn(on => !on);
    if (onChange) onChange();
  }

  
  const isDisabled = disabled ? " is-disabled" : "";
  const isChecked = on ? "is-checked" : "";

  const classes = ["Toggle", isChecked, isDisabled];
  return (
    <div className={tidy(classes)}>
      {label ? <label className="Toggle-label Label">{label}</label> : null}
      <div className="Toggle-container">
        <button 
          className="Toggle-background" 
          onClick={Switch}
        >
          <span className="Toggle-thumb" />
        </button>
        <label className="Label">
          {isChecked ? `${onText}` : `${offText}`}
        </label>
      </div>
    </div>
  );
}
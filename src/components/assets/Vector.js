import React from "react";

const Vector = ({ type, ...others }) => {
  const Image = type ? eval(type) : _default;
  return <div dangerouslySetInnerHTML={{ __html: Image }} {...others} />;
};

export default Vector;

const _default = `
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" focusable="false">
    <path d="M7.4246212,8.13172798 L2.12132034,2.82842712 L2.82842712,2.12132034 L8.13172798,7.4246212 L13.4350288,2.12132034 L14.1421356,2.82842712 L8.83883476,8.13172798 L14.1421356,13.4350288 L13.4350288,14.1421356 L8.13172798,8.83883476 L2.82842712,14.1421356 L2.12132034,13.4350288 L7.4246212,8.13172798 Z" />
  </svg>
`;

export const binoculars = `
  <svg xmlns="http://www.w3.org/2000/svg" width="128" height="128" viewBox="0 0 128 128">
    <g fill="none" fill-rule="evenodd" stroke="currentColor" transform="translate(4 24)">
      <circle cx="24" cy="60" r="24" stroke-width="2"/>
      <circle cx="96" cy="60" r="24" stroke-width="2"/>
      <circle cx="60" cy="56" r="8" stroke-width="2"/>
      <path stroke-width="2" d="M118,50.4 L108.8,24.8 C106,18 99.6,12.8 92,12 L92,12 C92,5.2 86.8,0 80,0 C73.2,0 68,5.2 68,12 L52,12 C52,5.2 46.8,0 40,0 C33.2,0 28,5.2 28,12 L28,12 C20.4,12.8 14,17.6 11.2,25.2 L2,50.8"/>
    </g>
  </svg>
`;

export const appLoadError = `
  <svg xmlns="http://www.w3.org/2000/svg" width="128" height="128" viewBox="0 0 128 128">
    <g fill="none" fill-rule="evenodd" stroke="currentColor" stroke-linecap="round">
      <g class="vector-animated round" stroke-linejoin="round" transform="translate(8 4)">
        <path stroke-width="2" d="M108.909091 32.1818182C99.0909091 13.0909091 78.9090909 0 56 0 30.3636364 0 9.09090909 15.8181818.363636364 38.1818182M2.54545455 87.2727273C12.3636364 106.363636 32.5454545 120 56 120 81.6363636 120 102.909091 104.181818 111.636364 81.8181818"/>
        <polyline stroke-width="2" points="110.545 0 110.545 32.727 77.818 32.727"/>
        <polyline stroke-width="2" points="1.455 120 1.455 87.273 34.182 87.273"/>
      </g>
      <path stroke-width="2" d="M64 40L64 68M64 84L64 92"/>
    </g>
  </svg>
`;

export const notify = `
  <svg xmlns="http://www.w3.org/2000/svg" width="128" height="128" viewBox="0 0 128 128">
    <g fill="none" fill-rule="evenodd" stroke="currentColor" transform="translate(12 24)">
      <polygon stroke-width="2" points="12 52 0 52 0 28 12 28 104 0 104 80"/>
      <path stroke-width="2" d="M48,63.2 L46.8,67.2 C44.8,74 37.6,77.6 31.2,75.6 L31.2,75.6 C24.4,73.6 20.8,66.4 22.8,60 L24,56"/>
    </g>
  </svg>
`;

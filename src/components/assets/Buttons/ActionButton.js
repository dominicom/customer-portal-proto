import React from "react";

import Icon from "../Icon";

export default function ActionButton({
  onClick,
  label,
  icon,
  kind,
  className,
  ...others
}) {
  const primaryButton = kind === "primary" ? " action-Button--primary" : "";
  const classes = [`action-Button${primaryButton}`, className ].join(" ");
  return (
    <button className={classes} onClick={onClick} {...others}>
      <Icon className="Icon" type={icon} />
      {label ? <div className="action-Button-label">{label}</div> : null}
    </button>
  );
}

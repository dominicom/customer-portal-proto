import React from "react";

import Icon from "components/assets/Icon";

import { tidy } from "utils/Lib";

import "./Button.scss";


export default function Button({
  onClick,
  label,
  kind,
  hero,
  onGray,
  primary,
  disabled
}) {
  const primaryButton = kind === "primary" || primary ? "Button--primary" : "Button--default";
  const isDisabled = disabled ? "is-disabled" : "";
  const isDarker = onGray ? "on-gray" : "";

  const classes = ["Button", primaryButton, isDisabled, isDarker];
  return (
    <button className={tidy(classes)} onClick={!disabled ? onClick : null}>
      {hero ? <Icon className="Icon" type={hero.icon} /> : null}
      {label ? <div className="Button-label">{label}</div> : null}
    </button>
  );
}

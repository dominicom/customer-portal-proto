import React from "react";

import * as Func from "utils/Lib";
import { tidy } from "utils/Lib";

import "./AccountItem.scss";

const AccountItem = ({ name, onClick, selected }) => {
  //const isActive = selected ? " is-active" : "";
  const isActive = name === selected ? " is-active" : "";
  //console.log(name, typeof name);
  const Color = Func.GetColor(name);
  //console.log(Color);
  const classes = ["cp-AccountItem", isActive]
  return (
    <div className={tidy(classes)} onClick={onClick} title={name}>
      <div className="cp-AccountItem-persona">
        <div
          className="icon-container"
          style={{ backgroundColor: Color }}
        >
          <div className="Initials">
            <span>{Func.GetInitials(`${name}`)}</span>
          </div>
        </div>
      </div>
      <div
        className="cp-AccountItem-label"
        //ariaLabel={`Select company ${name}`}
      >
        <span>{name}</span>
      </div>
    </div>
  );
};
export default AccountItem;

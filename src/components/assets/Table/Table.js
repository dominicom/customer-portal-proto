import React, { useEffect, useState, useRef } from "react";

import Pagination from "components/assets/Pagination/Pagination";
import Icon from "components/assets/Icon";
import ActionButton from "components/assets/Buttons/ActionButton";

import { tidy } from "utils/Lib";

import "./Table.scss";

const Table = ({ children, detailed }) => {
  const classes = ["cp-Table", detailed ? "DetailList" : "List"];
  return (
    <div
      className={tidy(classes)}
    >
      {children}
      <Pagination />
    </div>
  );
};

export default Table;

export const Header = ({ children, detailed, icon, iconPlaceholder, tile }) => {
  const classes = ["cp-Table-Head-row", tile ? "TileItem" : ""];
  return (
    <div className="cp-Table-Head">
      <div 
        className={tidy(classes)}
      >
        {icon ? <CellIcon icon={icon} /> : null}
        {iconPlaceholder ? <div className="iconColumn is-empty" /> : null}
        {children}
      </div>
    </div>
  );
}

export const Body = ({ children, detailed }) => (
  <div className="cp-Table-Body">
    {children}
  </div>
);

export const Row = ({ children, link, icon, tile }) => {
  const node = useRef();
  const [focus, setFocus] = useState(false);

  const Focus = () => {
    setFocus(!focus)
  }

  const classes = ["cp-Table-Row", link ? "listItem" : "", focus ? "is-selected" : "", tile ? "TileItem" : ""];
  const isFocusable = link ? null : true;
  const RowElement = link ? "a" : "div";

  const handleClickOutside = e => {
    if (node.current.contains(e.target)) {
      // inside click
      return;
    }
    // outside click
    setFocus(false);
  };

  useEffect(() => {
    if (focus) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [focus]);
 


  return (
    <RowElement ref={node} className={tidy(classes)} role="presentation" onClick={link ? link : Focus} data-is-focusable={isFocusable} focusable="true">

      {icon ? <CellIcon icon={icon} /> : null}

      {children}
    </RowElement>
  );
}


export const Cell = ({ children, onClick, type }) => {
  const CellElement = type === "title" ? "div" : "span";
  const cellClass = type === "title" ? "TitleColumn" : null;
  return (
    <div 
      className="cp-Table-Cell" 
      role="gridcell" 
      data-table-type={type ? type : null}
    >
      <CellElement className={cellClass}>
        {children}
      </CellElement>
    </div>
  );
}
export const ItemName = ({ title, link, mainAction}) => {
  const Action = mainAction ? (
    <div className="main-action">
      <ActionButton icon={mainAction ? mainAction.icon : null} onClick={mainAction ? mainAction.link : null} />
    </div>
  ) : null;

  
  

  const Element = link ? (
    <>
      <span className="row-link" role="link" onClick={link} title={title}>
          {title}
      </span>
      {Action}
    </>
  ) : (
    <>
      <span className="item-title" title={title}>
        {title}
      </span>
      {Action}
    </>
  );

  return Element;
}

const CellIcon = ({ icon }) => (
  <div className="row-icon" role="gridcell" data-icon={icon}>
    <Icon className="Icon" type={icon ? icon : "file"} />
  </div>
)

export const TileField = ({ children }) => (
  <div className="TileField">{children}</div>
)
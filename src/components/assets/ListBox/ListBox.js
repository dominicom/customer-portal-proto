import React, { Component } from "react";

import AccountItem from "components/assets/AccountItem/AccountItem";

import "./ListBox.scss";

export default class AccountListBox extends Component {
  render() {

    const { accounts, selectedAccount, SelectAccount } = this.props;
    console.log("ListBox:", selectedAccount);

    return (
      <ListBox>
        <Heading />

        {/* SELECTED ITEM AS FIRST ON THE LIST  */}
        <ul id="accounts-list" className="listBox" role="listbox">
          {/* IF SELECTED ANY ACCOUNT... */}
          {selectedAccount ? (
            <li className="listBox-item">
              <AccountItem
                name={selectedAccount.name}
                selected={selectedAccount.name}
              />
            </li>
          ) : null}
          {accounts
            .filter(acc => acc.name !== selectedAccount.name)
            .map((item, i) => (
              <li key={i} className="listBox-item">
                  <AccountItem
                    name={item.name}
                    onClick={() => SelectAccount(item)}
                    selected={selectedAccount.name}
                  />
              </li>
            ))}
        </ul>
      </ListBox>
    );
  }
}
  
const ListBox = ({ children }) => (
  <div className="cp-Account-ListBox">
    <div className="accounts-container popup">
      {children}
    </div>
  </div>
)
const Heading = () => (
  <div id="account-header" className="cp-Account-ListBox-header">
    <h4 className="heading-semibold">Change client</h4>
  </div>
)
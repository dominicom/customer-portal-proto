import React from "react";


import Icon from "components/assets/Icon";



export const IconButton = ({ type }) => (
  <div className="header-Button--icon">
    <Icon className="Icon" type={type} />
  </div>
)

export const UserPersonaBadge = () => (
  <button id="user"> 
    <div className="userPersonaButton">
      <Icon type="persona" />
    </div>
  </button>
)
import React, { Component } from "react";
import { PortalContext } from 'ContextStore';
// import MessageBar from "../MessageBar/MessageBar";

import Icon from "components/assets/Icon";

//eslint-disable-nextline
import { IconButton, UserPersonaBadge } from "./HeaderButtons";

import "./Header.scss";



const headerButtons = [
  // {
  //   label: "Contact",
  //   item: <IconButton type="contactIcon" />,
  //   component: null,
  // },
  {
    label: "User Panel",
    item: <UserPersonaBadge />,
    component: null,
  },
]

class Header extends Component {
  render() {
    const { Consumer } = PortalContext;
    return (
      <header>
        <div className="header-content">
          <Logo 
            //defaultLogo 
          />

          <RightRegion>

              <Consumer>
                {context => 

                    <ul>
                      {headerButtons.map(button =>  
                        <li key={button.label}>
                          <HeaderButton                     
                            onClick={() => context.openTaskPane()} 
                            item={button.item}
                            active={context.isTaskPaneActive}
                          />
                        </li>
                      )}
                    </ul>

                }
              </Consumer>

          </RightRegion>
        </div>
      </header>
    );
  }
}
export default Header;




const HeaderButton = ({ onClick, active, item }) => {
  const classes = `header-Button${active ? " is-active" : ""}`
  return (
    <div className={classes} onClick={onClick}>
      {item}
    </div>

  )
}




const Logo = ({ defaultLogo }) => {
  const Custom =     
    <div id="logo" className="client-logo">
      <a href="/">
        <span>Law & Rence</span>
      </a>
    </div>;
  const DefaultLogo = 
    <a href="/">
      <div id="logo" className="default-logo">
        <Icon className="cp-logo-icon" type="organisation" />
      </div>
    </a>;
  if (defaultLogo) {
    return DefaultLogo;
  } else {
    return Custom;
  }
}


const RightRegion = ({ children }) => (
  <div className="header-actions product-section">
    <div className="product-name">
      <span>Advisor247 Portal</span>
    </div>
    {children}
  </div>
)
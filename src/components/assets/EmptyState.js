import React from "react";

import Icon from "./Icon";
import Vector from "./Vector";

import "./EmptyState.scss";

export default ({ type, image, ...other }) => {
  const Component =
    type === "simple" ? <Empty image="appLoadError" {...other} /> : null;
  return (
    <>
      <Empty image={image} {...other} />
    </>
  );
};

export const Empty = ({ page, image, primary, warning, title, subtext }) => (
  <div
    className={`EmptyState ${primary ? "alert-primary" : ""} ${
      warning ? "alert-warning" : ""
    }`}
  >
    <Vector className="EmptyState-image" type={image ? image : null} />
    <div
      className="EmptyState-title"
      style={{ textAlign: `center`, marginBottom: 20 }}
    >
      {title ? title : "Empty state without any message and call to action!"}
    </div>
    <div className="EmptyState-subtext">
      {subtext ? subtext : "and any subtext too!"}
    </div>
  </div>
);
const Simple = ({ page, image, primary, children }) => (
  <section>
    <article
      style={{
        display: `flex`,
        flexDirection: `column`,
        alignItems: `center`,
        padding: `2rem`
      }}
    >
      <Icon
        style={{
          width: `2rem`,
          margin: `0 0 2.5rem`,
          fill: primary ? `#2E668B` : `#333`
        }}
        type={image ? image : null}
      />
      <div style={{ textAlign: `center`, marginBottom: 40 }}>
        {children
          ? children
          : "Empty state without any message and call to action!"}
      </div>
    </article>
  </section>
);

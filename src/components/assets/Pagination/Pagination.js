import React, { Component } from "react";

import Dropdown from "../Dropdown/Dropdown";
import ActionButton from "../Buttons/ActionButton";

import "./Pagination.scss";

export default class Pagination extends Component {
  render() {
    const pageAmount = Array.from(Array(5).keys(x => x + 1));
    return (
      <div className="cp-Table-pagination">
        <div className="Pagination">
          <div className="Pagination-pageManagement">
            <ActionButton icon="chevronLeft" />
            <p>Page</p>
            <Dropdown options={pageAmount.map(x => x + 1)} />
            <p>of {pageAmount.length}</p>
            <ActionButton icon="chevronRight" />
          </div>
          <div className="Count">
            <p>Items</p>
            <p>1-20</p>
            <p>of 100</p>
          </div>
        </div>
      </div>
    );
  }
}

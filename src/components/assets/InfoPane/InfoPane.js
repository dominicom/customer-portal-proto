import React from "react";

import "./InfoPane.scss";

const InfoPane = ({ title, children }) => {
  return (
    <div className="cp-InfoPane">
      {title ? (
        <div className="cp-InfoPane-header">
          <h4 className="heading-semibold">{title}</h4>
        </div>
      ) : null}

      <dl className="cp-InfoPane-section cp-InfoPane-infoData">{children}</dl>
    </div>
  );
};
export default InfoPane;

export const InfoData = ({ data }) => {
  return (
    <>
      <dt className="cp-InfoPane-infoData-title">{data.title}</dt>
      <dd className="cp-InfoPane-infoData-desc">{data.desc}</dd>
    </>
  );
};

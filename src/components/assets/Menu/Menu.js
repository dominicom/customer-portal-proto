import React, { Component } from "react";
import  { NavLink } from "react-router-dom";
import { PortalContext } from 'ContextStore';

import AccountSection from "./AccountSection";
import ListBox from "components/assets/ListBox/ListBox";
import Icon from "components/assets/Icon";

import ActionButton from "components/assets/Buttons/ActionButton";

import { tidy } from "utils/Lib";

import "./Menu.scss";
import "./AccountSection.scss";

const BREAK_POINT = 768;
const MIN_HEIGHT = 450;



const items = [
  { page: "dashboard", icon: "home", label: "Dashboard" },
  { page: "cases", icon: "cases", label: "Cases" },
  { page: "documents", icon: "document", label: "Documents" },
  { page: "invoices", icon: "invoice", label: "Invoices" },
  { page: "tasks", icon: "tasks", label: "Tasks" },
];

// How to manage React State with Arrays
// https://www.robinwieruch.de/react-state-array-add-update-remove




// HOC withContext
// https://stackoverflow.com/questions/54998757/how-to-compare-previous-context-with-current-context-in-react-16-x
const withContext = Wrapped => props => {
  const { Consumer } = PortalContext; 
  return (
    <Consumer>
        {context => <Wrapped {...props} contextValue={context}/>}
    </Consumer>
  );
}

class Menu extends Component {


  constructor() {
    super();
    this.state = {
      expanded: true,
      accountPanel: false,
      width: 0,
      overflowSet: false,
      // isMobile: "",
      // accounts: [],
      // selectedAccount: null
    };
    this.actionMenuHandle = this.actionMenuHandle.bind(this);
    this.actionExpandAccountSection = this.actionExpandAccountSection.bind(
      this
    );
  }
  componentDidMount() {
    let context = this.props.contextValue
    this.setState({ accounts: context.client.accounts, selectedAccount: context.client.selectedAccount }, () => console.log(this.state.selectedAccount));
    console.log("propsy z kontekstu",this.props)
    

    this.WindowSize(); // Set width
    window.addEventListener("resize", this.WindowSize);


    //let context = this.context
    this.setState({ isMobile: context.mobile }, () => console.log("isMobile:", this.state.isMobile));

    
    //console.log("context:", this.context.mobile)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.WindowSize);
  }

  componentDidUpdate(prevProps) {
    let context = this.props.contextValue
    if (prevProps.contextValue !== context) {
      this.setState({ selectedAccount: context.client.selectedAccount }, () => console.log(this.state.selectedAccount));
    }


    let Mobile = context.isMobile !== this.state.mobile;

    if (Mobile) {
      console.log(Mobile, context.isMobile); 
      this.setState({ isMobile: context.mobile });
    }
  }

  WindowSize = () => {
    let width = window.innerWidth;
    let height = window.innerHeight;

    const { expanded } = this.state;

    // const isMobile = this.props.contextValue.mobile;
    const compactMode = this.props.contextValue.compactMode.menu;

    // @PARAM BREAK_POINT     - number, minimum resolution width when menu is for mobile view (contracted)
    // @see top of file
    width < BREAK_POINT
      ? this.setState({ isMobile: true })
      : this.setState({ isMobile: false });


    // This conditions are NICE TO HAVE
    // This feature switchs menu to "collapsed" state when window size is changed and less than minimum resolution
    // @see BREAK_POINT
    // It prevents showing expanded menu by default when resolution is less than BREAK_POINT parameter.

    // @PARAM expanded        - state of this component controlling collapsed or expanded menu

    // DISCLAIMER: This is only ordinary logic for testing functionality, not ready solution for this.

    /*
    if (isMobile && expanded) {
      this.setState({ expanded: false });
    }
    if (!isMobile && !expanded) {
      this.setState({ expanded: true })
    }
    */
    if (compactMode && expanded) {
      this.setState({ expanded: false });
    }
    if (!compactMode && !expanded) {
      this.setState({ expanded: true })
    }

    this.setState({ width: width });

    
    height < MIN_HEIGHT
      ? this.setState({ overflowSet: true })
      : this.setState({ overflowSet: false });

  };

  actionMenuHandle = () => {
    this.setState(prevState => ({
      expanded: !prevState.expanded,
      // TO-DO: Consider the right behaviour
      accountPanel: this.state.accountPanel ? false : this.state.accountPanel
    }));
  };
  actionExpandAccountSection = () => {
    this.setState(prevState => ({
      accountPanel: !prevState.accountPanel
    }));
  };
  actionMenuItemClick = page => {
    //this.props.SwitchPageView(page);

    // This condition is used to collapse Menu when its expandes on small resolutions (ie. mobile devices)
    // It prevents to covering view by Menu.

    // SHORTLY:
    // @Case:  on mobile devices "onClick" event Menu should be collapsed
    if (this.state.isMobile && this.state.expanded) { 
      this.actionMenuHandle(); 
    }
  };


  SelectAccount = account => {
    let context = this.props.contextValue;
    this.setState({ selectedAccount: account });
    console.log("Selekted Akant:", account);
    context.SelectClientAccount(account);
    // Invoking this function collapse Account Section after selecting account
    this.actionExpandAccountSection();
  };

  render() {
    const { SelectAccount, actionMenuItemClick, actionMenuHandle, actionExpandAccountSection } = this;
    const {
      expanded,
      overflowSet,
      //isMobile,
      accountPanel,
      accounts,
      selectedAccount
    } = this.state;

    const isMobile = this.props.contextValue.mobile;
    const isOverflowed = this.props.contextValue.compactMode.menu;

    const accountSelectorProps = {
      accounts: accounts,
      selectedAccount: selectedAccount,
      SelectAccount: SelectAccount,
      actionExpandAccountSection: actionExpandAccountSection,
      accountPanel: accountPanel,
      expanded: expanded
    }

    const classes = {
      nav: ["cp-Navigation", isOverflowed ? "overflowed" : ""],
      menu: ["menu-sidebar",
      expanded ? "" : "collapsed",
      isMobile ? "menu-sidebar--mobile" : "",
      accountPanel ? " noscrollable" : "",]
    };

    return (

      <nav id="menu" className={tidy(classes.nav)}>
        <div className={tidy(classes.menu)}>

          <Hamburger actionMenuHandle={actionMenuHandle} />

          <AccountSection {...accountSelectorProps} />

          {!overflowSet ? (

              <div id="menu-items" className="menu-items">
                {items.map((item, i) => (
                  <MenuItem key={i} {...item} onClick={() => actionMenuItemClick()}/>
                ))}
              </div>

          ) : (
            <>
              {expanded ? (
                <div id="menu-items" className="menu-items">
                  <MenuItem page="dashboard" icon="home" label="Dashboard" />
                </div>
              ) : (
                <div className="menu-items">
                  <Overflow />
                </div>
              )}
            </>
          )}


        </div>
        {!expanded && accountPanel ? (
          <ListBox {...accountSelectorProps} />
        ) : null}
      </nav>

    );
  }
}
Menu.contextType = PortalContext;
export default withContext(Menu);

const Hamburger = ({ actionMenuHandle }) => {
  return (
    <button
      id="hamburger"
      className="Hamburger menu-Button Button--secondary"
      onClick={actionMenuHandle}
    >
      <Icon type="hamburger" />
    </button>
  );
};

const Overflow = ({ actionMenuHandle, overflowSet }) => {
  return (
    <button
      id="overflowedMenu"
      className="OverflowMenu menu-Button Button--secondary"
      onClick={actionMenuHandle}
    >
      <Icon type="more" />
    </button>
  );
};


const MenuItem = ({ ...item }) => {
  return (
    <div className="menu-item">
      <NavLink
        strict
        to={{ 
          pathname: `/${item.page}`, 
          state: { page: item.page }}}
        className="menu-Button"
        activeClassName="is-active"
      >
        <Icon type={item.icon} />
        <span className="menu-Button--label">{item.label}</span>
      </NavLink>
    </div>
  );
};

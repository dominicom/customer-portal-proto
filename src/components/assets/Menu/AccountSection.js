import React, { Component } from "react";

import AccountItem from "components/assets/AccountItem/AccountItem";
import Icon from "components/assets/Icon";

import { tidy } from "utils/Lib";
//import MessageBar from "../MessageBar/MessageBar";

class AccountSection extends Component {

  render() {
    const { actionExpandAccountSection, accountPanel, expanded, accounts, selectedAccount, SelectAccount } = this.props;
    const chevron = expanded
      ? accountPanel
        ? "chevronUp"
        : "chevronDown"
      : accountPanel
      ? "chevronLeft"
      : "chevronRight";

    const classes = {
      root: ["cp-AccountSection", accountPanel && expanded ? " expanded" : ""],
      heading: `cp-AccountSection-heading`
    };
    const titles = {
      focusArea: ["Expand list", selectedAccount !== null ? " and change account" : " to select account"]
    }

    return (

            <div className={tidy(classes.root)}>
              <FocusArea
                title={titles.focusArea}
                onClick={() => actionExpandAccountSection()}
              />
              <Heading className={classes.heading} chevron={chevron} />
 

              <div className="account-selected">
                {selectedAccount ? (
                  <AccountItem name={selectedAccount.name} />
                ) : (
                  <MessageBar expanded={expanded} />
                )}
              </div>
              {expanded && accountPanel ? (
                <Slider>
                      {accounts
                        .filter(acc => acc.name !== selectedAccount.name)
                        .map((item, i) => (
                          <li key={i}>
                            <AccountItem
                              name={item.name}
                              onClick={() => SelectAccount(item)}
                            />
                          </li>
                        ))}
                </Slider>
              ) : null}
            </div>
    );
  }
}

export default AccountSection;

const FocusArea = ({ ...props }) => (
  <div
    className="focusArea"
    {...props}
  />
)

const Heading = ({ className, chevron }) => (
  <div className={className}>
    <div className="account-header">
      <h4 className="heading-semibold">Client</h4>
    </div>
    <div className="panelButton">
      <Icon className="Icon" type={chevron} />
    </div>
  </div>
);

const MessageBar = ({ expanded }) => {
  const Label = 
    <div className="MessageBar-label">
      <span>No account selected</span>
    </div>;
  
  return (
    <div className="cp-AccountSection-MessageBar">
      <div className="MessageBar-container">
        <Icon className="Icon" type="info" />
      </div>
      {expanded ? Label : null}
    </div>
  );
}




const Slider = ({ children }) => (
  <>
    <div className="accounts-container slider">
      <ul id="accounts-list" className="slideDown">
        {children}
      </ul>
    </div>
    <div className="shade" />
  </>
)
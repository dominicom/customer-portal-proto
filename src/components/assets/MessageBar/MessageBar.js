import React from "react";
import Icon from "../Icon";

import "./MessageBar.scss";

const MessageBar = ({ warn, children }) => {
  return (
    <div className="MessageBar">
      <Icon className="Icon" type="info" />
      <div style={{ margin: `0 0.5rem` }}>{children}</div>
    </div>
  );
};

export default MessageBar;

import React, { Component } from "react";
import { PortalContext } from 'ContextStore';
import { tidy } from 'utils/Lib';

import ActionButton from "components/assets/Buttons/ActionButton";

import "./TaskPane.scss";


export default class TaskPane extends Component {
  state = {
    open: false
  }
  componentDidMount () {
    const { open } = this.props;
    setTimeout(() => { 
      this.setState({ open: open }); 
    }, 50);
  }
  componentDidUpdate(prevProps) {
    const { open } = this.props;
    const stateChanged = this.props.open !== prevProps.open;
    if (stateChanged) {
      this.setState({ open: open });
    }  
  }
  render () {
    const { open } = this.state;
    const { children, content, className, title } = this.props;
    const classes = ["cp-TaskPane", className ? className : "", open ? "open" : ""];
    return (
      <div className="TaskPane-container">
        <div className={tidy(classes)} role="region">
          <TopBar title={title} />
          <Content content={content} >
            {children}
          </Content>
          <Footer />
        </div>
      </div>
    )
  }
}

const TopBar = ({ title, onClick }) => {
  const { Consumer } = PortalContext;
  return (
    <div className="cp-TaskPane-topbar flex-101">
      <h2 className="heading-light">{title}</h2>
      <Consumer>
            {context => <ActionButton className="close-pane" onClick={() => context.openTaskPane()} />}
      </Consumer>
    </div>
  )
}

const Content = ({ children, content }) => (
  <div className="cp-TaskPane-content">
    {children}
    {content}
  </div>
)


const Footer = () => (
  <div className="cp-TaskPane-footer">
  <span>Advisor247 Portal - ver 1.0.0</span>
</div>
)
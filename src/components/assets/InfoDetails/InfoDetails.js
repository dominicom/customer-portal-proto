import React from "react";

import ActionButton from "../Buttons/ActionButton";

import "./InfoDetails.scss";

const InfoDetails = ({ title, children }) => {
  return (
    <section className="cp-InfoDetails">
      {title ? (
        <div className="cp-InfoDetails-header">
          <h2 className="heading-light">{title}</h2>
        </div>
      ) : null}

      <div className="cp-InfoDetails-section cp-InfoDetails-infoData">
        {children}
      </div>
    </section>
  );
};
export default InfoDetails;

export const InfoDetailsItem = ({ data }) => {
  return (
    <div className="cp-InfoDetails-item">
      <div className="cp-InfoDetails-itemTitle">{data.title}</div>
      <div className="cp-InfoDetails-itemDesc">{data.desc}</div>
    </div>
  );
};

export const InfoDetailsItemClickable = ({ data, alt, tooltip }) => {
  return (
    <div
      className="cp-InfoDetails-itemLink"
      role="link"
      alt={alt}
      title={alt}
      aria-label={alt}
    >
      <div className="cp-InfoDetails-itemTitle">{data.title}</div>
      <div className="cp-InfoDetails-itemDesc">{data.desc}</div>
      <div className="cp-InfoDetails-itemAction command--hide">
        <ActionButton icon="copy" alt={tooltip} title={tooltip} />
      </div>
    </div>
  );
};

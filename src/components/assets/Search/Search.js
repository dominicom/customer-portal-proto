import React, { Component } from "react";
import { DebounceInput } from "react-debounce-input";

import ActionButton from "../Buttons/ActionButton";
import Icon from "../Icon";

import "./Search.scss";

// https://codepen.io/graubnla/pen/EgdgZm

class Search extends Component {
  state = {
    value: "",
    active: false
  };
  render() {
    const { value, active } = this.state;

    const classes = {
      searchBox: `cp-SearchBox-field${active ? " is-active" : ""}`
    };

    return (
      <div
        id="search"
        className={classes.searchBox}
        onFocus={event => this.setState({ active: true })}
        onBlur={event => this.setState({ active: false })}
      >
        <div className="icon-container">
          <Icon type="search" className="Icon" />
        </div>
        <DebounceInput
          className="cp-SearchBox-field"
          placeholder="Search"
          value={value}
          //minLength={2}
          debounceTimeout={300}
          onChange={event => this.setState({ value: event.target.value })}
          inputRef={ref => {
            this.ref = ref;
          }}
        />
        {value !== "" ? (
          <div className="cp-SearchBox-clearButton">
            <ActionButton
              onClick={event => {
                this.setState({ value: "" });
                this.ref.focus();
              }}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default Search;

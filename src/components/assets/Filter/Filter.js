//eslint-disable-nextline
import React, { useEffect, useState, useRef } from "react";
import Icon from "components/assets/Icon";

import "./Filter.scss";

const Filter = ({ label, icon }) => {
  return (
    <div className="Filter">
      <div className="Filter-Title">{label}</div>

      <Icon className="Icon" type="chevronDown" />
    </div>
  );
};
export default Filter;
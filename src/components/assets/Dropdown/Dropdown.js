import React, { Component } from "react";
 // eslint-disable-next-line
import Icon from "../Icon";

import "./Dropdown.scss";

// Custom Dropdown w3schools
// https://www.w3schools.com/howto/tryit.asp?filename=tryhow_custom_select

// Other nice one - codesandbox
// https://codesandbox.io/s/9o3lw5792w

// References
// https://www.carbondesignsystem.com/components/dropdown/code
// https://developer.microsoft.com/en-us/fabric-js/components/dropdown/dropdown
// https://developer.microsoft.com/en-us/fabric#/controls/web/dropdown

const defaults = {
  options: ["option #1", "option #2"]
};

export default class Dropdown extends Component {
  render() {
    const { label, placeholder } = this.props;
    const options = this.props.options ? this.props.options : defaults.options;
    // const placeholder = this.props.placeholder
    //   ? this.props.placeholder
    //   : "Select...";
    return (
      <div className="Dropdown" tabIndex="0">
        {label ? <label className="Label">{label}</label> : null}

        {/* TO-DO Custom drop */}
        {/* <Icon className="Icon" type="chevronDown" /> */}
        <select className="Select">
          {placeholder ? <option>{placeholder}</option> : null}
          {options.map((o, i) => (
            <option key={i}>{o}</option>
          ))}
        </select>
      </div>
    );
  }
}

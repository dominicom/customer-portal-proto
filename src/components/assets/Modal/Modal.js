import React, { Component } from "react";
import { PortalContext } from 'ContextStore';
//import { tidy } from 'utils/Lib';

import ActionButton from "components/assets/Buttons/ActionButton";

import "./Modal.scss";

export default class Modal extends Component {
  state = {
    open: false
  }
  render () {
    const { title, close, children, component } = this.props;
    return (
      <ModalCore>
        <ModalCommandBar 
          title={component.title ? component.title : "Modal title"} 
        />
        <ModalBody>
          {component.content ? component.content : <p>Lorem Ipsum Modal</p>}
          {children}
        </ModalBody>
        {/* <ModalButtonGroup /> */}
      </ModalCore>
    )
  }
}
const ModalCore = ({ children }) => (
  <div className="cp-Layer">
    <div className="cp-Overlay"></div>
    <div className="cp-Modal UploadFile">
      {children}
    </div>
  </div>
)

const ModalCommandBar = ({ title }) => {
  const { Consumer } = PortalContext;
  return (
    <div className="commandBar">
      <div className="cp-Modal-title">
        <h2>{title}</h2>
      </div>
      <div className="closeButton">
        <Consumer> 
          {context => 
              <ActionButton className="close-modal" onClick={() => context.closeModal()} />
          }
        </Consumer>
      </div>
    </div>
  )
}

const ModalBody = ({ children }) => (
  <div className="cp-Modal-body">
    {children}    
  </div>
)


const ModalButtonGroup = ({ children }) => (
  <div className="cp-Button-group">
    {children}
  </div>
)
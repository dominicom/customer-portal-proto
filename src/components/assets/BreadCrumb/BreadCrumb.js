import React from "react";
import Icon from "../Icon";

import "./BreadCrumb.scss";

const BreadCrumb = ({ page, crumbs, title, SwitchPageView }) => {
  return (
    <div className="Breadcrumb">
      <ul className="Breadcrumb-list ms-Breadcrumb-list">
        {crumbs
          ? crumbs.map(c => (
              <Item key={c} crumb={c} onClick={event => SwitchPageView()} />
            ))
          : ["Crumb #1", "Crumb #2"].map(c => <Item key={c} crumb={c} />)}
        <li className="Breadcrumb-listItem ">
          <div className="Breadcrumb-title">{title}</div>
        </li>
      </ul>
    </div>
  );
};

export default BreadCrumb;

const Item = ({ crumb, onClick }) => {
  return (
    <li className="Breadcrumb-listItem ms-BreadCrumb-listItem">
      <a className="Breadcrumb-link ms-Breadcrumb-itemLink" onClick={onClick}>
        {crumb}
      </a>
      <Icon className="Breadcrumb-chevron" type="chevronRight" />
    </li>
  );
};

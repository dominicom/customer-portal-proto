import React from "react";
import Icon from "components/assets/Icon";

import { tidy } from "utils/Lib";

import "./Persona.scss";

const Persona = ({ label, desc, kind, initials, icon }) => {
  const Size =  kind === "large"  ? "large"  : null || 
                kind === "medium" ? "medium" : null || 
                kind === "small"  ? "small"  : null;
  const Image = icon ? icon : "persona";
  const Label = label ? (
    <div>
      <h4 className="heading">{label}</h4>
    </div>
  ) : null;

  const classes = {
    root:  ["PersonaContainer", desc ? desc : null ,"flex-100"],
    image: ["cp-Persona-image", Size ? Size : "small"],
  }

  return (
    <div className={tidy(classes.root)}>
      <div className={tidy(classes.image)}>
        <div className="icon-container">
          <Icon type={Image} />
        </div>
      </div>
      {Label}
    </div>
  )
}
export default Persona;
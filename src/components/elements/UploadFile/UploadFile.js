import React, { Component } from "react";
import { PortalContext } from 'ContextStore';

import AccountItem from "components/assets/AccountItem/AccountItem";
import Button from "components/assets/Buttons/Button";
import ActionButton from "components/assets/Buttons/ActionButton";
// import ActionButton from "components/assets/Buttons/ActionButton";
import Dropdown from "components/assets/Dropdown/Dropdown";

import Icon from "components/assets/Icon";

import "./UploadFile.scss"

const cases = ["Case #1 - Divorce Case", "Case #2 - Last Will Case"];

export default class UploadFile extends Component {
  constructor() {
    super();
    this.state = {
      isSelectedFile: false
    };
    //this.selectFile = this.selectFile.bind(this);
  }
  SelectFile = () => {
    this.setState(prevState => ({
      isSelectedFile: !prevState.isSelectedFile
    }));
  }


  render() {
    const { isSelectedFile } = this.state;
    const { Consumer } = PortalContext; 
    return (
      <Consumer>
        {context =>
          <Wrapper>
              <section className="selectedClient-section">
                <div className="selectedClient-section-content">
                  <label className="Label">Client</label>
                  <AccountItem name={context.client.selectedAccount.name}/>
                </div>
              </section>
              
              <section class="selectCases-section">  
                <div class="caseDropdown">
                  <Dropdown
                    placeholder="Select..."
                    label="Select case (optionally)"
                    options={cases}
                  />
                </div>
              </section>

              <section className="browseFiles-section">
                <div className="browseSection-MessageBar">
                  <div className="iconContainer">
                    <Icon className="Icon" type="info" />
                  </div>
                  <div className="messageContainer">
                    <p>Selected file will be sent to Case Manager.</p>
                    <p>You can upload 1 file only.</p>
                  </div>
                </div>
                
                <div className="browseButton">
                  <Button 
                    onClick={() => this.SelectFile()} 
                    label="Choose file"
                    primary  
                    disabled={isSelectedFile ? true : null}/>
                </div>

              </section >

              {isSelectedFile ? (
                <SelectedFilesSection>
                  <File onClick={() => this.SelectFile()} />
                </SelectedFilesSection>
              ) : null}

              <ModalSection name="AddComment">
                  <label className="Label">Add comment (optionally)</label>
                  <textarea 
                    className="TextField TextArea" 
                    placeholder="Add comment to the file" 
                    // disabled={!isSelectedFile ? true : false}
                  >
                
                  </textarea>
              </ModalSection>


              <ButtonGroup
                primaryAction={null}
                // secondaryAction={null} 
                disabled={!isSelectedFile ? true : null} 
              />


          </Wrapper>
        }
      </Consumer>
    );
  }
}

const ButtonGroup = ({ disabled, primaryAction }) => {
  const { Consumer } = PortalContext;
  return (
    <div className="cp-Button-group">
      <Consumer> 
        {context => <Button label="Cancel" onClick={() => context.closeModal()} /> }
      </Consumer>
      {disabled ? (
        <Button disabled label="Upload" hero={{ icon: "upload" }} />
      ) : (
        <Button primary label="Upload" onClick={primaryAction} hero={{ icon: "upload" }} />
      )}
    </div>
  );
};

const Wrapper = ({ children }) => children;

const SelectedFilesSection = ({ children }) => (
  <div  className="selectedFiles-section">
    {children}
  </div>
)


const File = ({ selectFile, onClick }) => {
  return (
      <div className="cp-FileTile selectedFile">
        <div className="selectedFile-FileLabel">
          <Icon className="selectedFile-FileLabel-icon Icon" type="textFile" />
          <div className="selectedFile-FileLabel-name">
            Uploaded file #1 wyuwyeuwyew weyuweywuey uwyeuwye wue yuweyu yw y
            ywueywuhddscn sbbc snvb snbvsndbv nvnsdvbnbv
            sncvxnzvscnbdsvbdsvfj.ext
          </div>
          <ActionButton icon="remove" onClick={onClick} />
        </div>
      </div>
  );
};


const ModalSection = ({ children, name }) => {
  const title = name ? name : "Untitled";
  const classes = {
    root: [ title , "-section"].join(""),
    content: [ title , "-section-content"].join("")
  }
  return (
    <section className={classes.root}>
      <div className={classes.content}>
        {children}
      </div>
    </section>
  );
}
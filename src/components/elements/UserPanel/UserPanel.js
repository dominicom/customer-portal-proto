import React, { useState } from "react";
import { PortalContext } from 'ContextStore';
import  { Link } from "react-router-dom";
import Toggle from "components/assets/Toggle/Toggle"
import "./UserPanel.scss";


const links = [
  { 
    title: "Personal data",
    page: "personaldata"
  },
  { 
    title: "My uploaded files",
    page: "myfiles"
  },
  { 
    title: "Contact",
    page: "contact"
  },
  { 
    title: "Log out",
    page: "logout"
  },
];

export default function UserPanel () {
  const [on, setOn] = useState(false);
  
  const SwitchDesignMode = () => {
    setOn(on => !on);
  }
  if (on) {
    document.designMode = "on";
  } else {
    document.designMode = "off";
  }

  // Context Consumer
  const { Consumer } = PortalContext;
  return (
    <>
      <div className="user-details">
        <div className="cp-Persona user-icon">
          <div className="Icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
              <path d="M10.284884,8.37759104 C13.3136161,9.40895788 15.5,12.4360519 15.5,16 L14.5,16 C14.5,12.1262278 11.5821874,9 8,9 C4.41781265,9 1.5,12.1262278 1.5,16 L0.5,16 C0.5,12.4360519 2.68638395,9.40895788 5.71511596,8.37759104 C4.38932372,7.59467683 3.5,6.15119476 3.5,4.5 C3.5,2.01471863 5.51471863,0 8,0 C10.4852814,0 12.5,2.01471863 12.5,4.5 C12.5,6.15119476 11.6106763,7.59467683 10.284884,8.37759104 Z M8,8 C9.93299662,8 11.5,6.43299662 11.5,4.5 C11.5,2.56700338 9.93299662,1 8,1 C6.06700338,1 4.5,2.56700338 4.5,4.5 C4.5,6.43299662 6.06700338,8 8,8 Z"/>
            </svg>
          </div>
        </div>
        <div className="user-data">
          <div className="user-data-name" title="Astrid Nielsen">Astrid Nielsen</div>
          <div className="user-data-email" title="astrid.nielsen@gmail.com">astrid.nielsen@gmail.com</div>
        </div>
      </div>
      <Consumer> 
              {context => 
                    <div className="control-links">
                      {links.map((link, i) => <Alink key={i} {...link} onClick={() => {
                        context.closeTaskPane()
                      }} />)}
                    </div>
              }
      </Consumer>



      <Consumer> 
              {context => 
                    <div className="temp-console">
                      <h2 className="heading">Temporary console</h2>

                      <Toggle 
                        label="Mobile mode"
                        onText="On" 
                        offText="Off"
                        onChange={() => context.toggleMobileMode()}
                        checked={context.mobile ? true : false}
                      />
                      {/* <Toggle 
                        label="Design mode"
                        onText="On" 
                        offText="Off"
                        onChange={() => context.SwitchDesignMode()}
                      /> */}
                    </div>
              }
      </Consumer>


    </>
  );
}

const Alink = props => (
  <Link 
    //strict
    to={{ 
      pathname: `/${props.page}`, 
      state: { page: props.page }}}
    className="cp-Link-primary"
    onClick={props.onClick}
  >
    {props.title}
  </Link>
);


// <a className="cp-Link-primary">{props.title}</a>
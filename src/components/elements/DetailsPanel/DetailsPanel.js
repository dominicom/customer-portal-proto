import React, { Component } from "react";

// eslint-disable-next-line
import EmptyState from "components/assets/EmptyState";

import "./DetailsPanel.scss";

export default class DetailsView extends Component {
  render() {
    return (
      <div className="cp-BasePage-detailsView">
        <div>
          <EmptyState image="binoculars"
            title="Details Panel placeholder"
            subtext="Details View is planned as future component to deliver quick actions and detailed information about selected item on the table" 
          />
        </div>
      </div>
    );
  }
}

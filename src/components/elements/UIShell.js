import React, { Component } from "react";
import { PortalContext } from 'ContextStore';
import { delayUnmounting } from 'utils/Lib';

import Header from "components/assets/Header/Header";
import Menu from "components/assets/Menu/Menu";

import TaskPane from "components/assets/TaskPane/TaskPane";
import Modal from "components/assets/Modal/Modal";



import UserPanel from "components/elements/UserPanel/UserPanel";



export default class UIShell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDetailsViewOpen: false,
      isUploadWindowOpen: this.props.settings.isUploadWindowOpen,
      isUserPanelOpen: false,
      page: ""
    };
  }
  componentDidMount() {
    console.log("UIShell:", this.props.page)
  }
  componentDidUpdate(prevProps) {
    console.log("UIShell:", this.props.page)
    const locationChanged =
      this.props.page !== prevProps.page;
      //console.log(locationChanged); 
    if (locationChanged) {
      this.setState({ page: this.props.page});
    }
  }

  render() {
    const { Consumer } = PortalContext;
    const { children } = this.props;

    return (
      <div className="app-root">
        <Header />
        <div className="body-container">
          <Menu />



          {children}

          <Consumer> 
            {context => 
            
            
              <>
                <TaskPaneMount
                  delayTime={150} 
                  isMounted={context.isTaskPaneActive}
                  open={context.isTaskPaneActive} 
                  title="Profile"
                  children={<UserPanel />}
                /> 


                {context.isModalOpen ? (
                    <Modal component={context.modalComponent} />
                ) : null}
              </>
            }
          </Consumer>          
        </div>
      </div>
    );
  }
}
const TaskPaneMount = delayUnmounting(TaskPane);
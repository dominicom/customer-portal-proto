import React, { Component } from "react";
import { PortalContext } from 'ContextStore';
import { tidy } from 'utils/Lib';

import CommandBar from "components/assets/CommandBar/CommandBar";
import DetailsPanel from "components/elements/DetailsPanel/DetailsPanel";



// Sample Structure:
// @See
// https://emea.flow.microsoft.com/manage/environments/Default-3b0364a9-e449-46b0-a021-ae2f78779ca5/solutions

class PageGenerator extends Component {

  render () {
    const { page, children, listPage } = this.props;
    const { Consumer } = PortalContext;
    const Heading = page.charAt(0).toUpperCase() + page.substr(1).toLowerCase();
    const classes = {
      content: {
        page: `${Heading}-PageContainer`,
        wrapper: `content-wrapper`
      }

    }
    return (
      <Consumer> 
        {context =>
          <div className="cp-BasePage content-container" data-list-page={listPage ? "true" : null}>
            
            {listPage ? (
              <CommandBar />
            ) : null}

            <div
              className={tidy([classes.content.wrapper, context.isDetailsViewOpen ? "details-open" : ""])}
            >




              <BasePageHeader title={Heading} />
              <div className="cp-BasePage-wrapper">
                <div className={classes.content.page}>
                  <div className="Page-content PageLayout">

                    {children}

                  </div>
                </div>
              </div>

              {listPage && context.isDetailsViewOpen ? (
                <DetailsPanel />
              ) : null}




            </div>
          </div>
        }
      </Consumer>
    );
  };
}

export default PageGenerator;


const BasePageHeader = ({ title }) => (
  <div className="cp-BasePage-header">
    <h2 className="cp-Heading heading-light">{title}</h2>
  </div>
)
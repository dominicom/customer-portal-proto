import React from "react";
import Icon from "../../components/assets/Icon";

const PlaceHolder = ({ label }) => {
  return (
    <div
      style={{
        display: `flex`,
        justifyContent: `middle`,
        alignItems: `center`,
        width: `100%`,
        minHeight: `3rem`,
        padding: `0.5rem`,
        background: `#eaeaea`
      }}
    >
      <Icon className="Icon" type="info" style={{ margin: `0 0.5rem` }} />
      <div>{label}</div>
    </div>
  );
};
export default PlaceHolder;

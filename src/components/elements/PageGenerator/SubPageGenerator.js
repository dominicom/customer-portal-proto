import React from "react";
// import Icon from "components/assets/Icon";
import BreadCrumb from "components/assets/BreadCrumb/BreadCrumb";

const SubPageGenerator = ({
  page,
  crumbs,
  children,
  title,
  SwitchPageView
}) => {
  const Crumbs = crumbs
    ? crumbs.map(c => c.charAt(0).toUpperCase() + c.substr(1).toLowerCase())
    : null;
  const Title = title
    ? title.charAt(0).toUpperCase() + title.substr(1).toLowerCase()
    : "Generic title";

  return (
    <div className={`contentPage-${page}`}>
      <div
        className="cp-BasePage-header"
        style={{
          display: `flex`,
          position: `sticky`,
          background: `white`,
          zIndex: 10,
          height: `3.5rem`,
          top: 0,
          paddingBottom: `0.5rem`
        }}
      >
        <BreadCrumb
          page={page}
          crumbs={Crumbs}
          title={Title}
          SwitchPageView={SwitchPageView}
        />
      </div>

      <div className="cp-BasePage-content">{children}</div>
    </div>
  );
};

export default SubPageGenerator;

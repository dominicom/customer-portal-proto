import React from "react";

const PageGenerator = ({ children }) => {
  return (
    <div className="cp-Page content-container">
      <div className="content-wrapper">
        <div className="Page-content PageLayout">{children}</div>
      </div>
    </div>
  );
};

export default PageGenerator;

# Customer Portal

This is **Customer Portal** User Interface Prototype build with React (using `create-react-app` starter) to test `HTML` and `CSS` structures and Front-end concepts using **Fluent Design Language** by _Microsoft_.

This project previously was developed using **Codesandbox**. Because of some performance & stability issues moved to local and deployed with **Heroku**.

It can be used partly as a _code giver_ for default project of Customer Portal Client developed by Advisor247 Dev Team but not all solutions in `React` sounds legit in `Angular 8` :-P

## Preview

Project works on [_Heroku_](https://www.heroku.com/) platform.

> **LINK**: https://customer-portal-proto.herokuapp.com/

## Installation Development Mode

**Steps**:
* Before instalation create own local repository independent from `customer-portal-ux`.

> **PATH**: ../customer-portal-ux/Sandbox Prototype/heroku-customer-portal-proto

* Installation Node Modules
```
cd heroku-customer-portal-proto
$ npm install
```
* Run React App
```
$ npm start
```
* Open browser
```
http://localhost:3000/
```


## Heroku Deployment

**Steps**:
* Login
```
$ heroku login
```
Use credentials of `dkiepuszewski@advisor247.pl` account.

* Git commit
```
$ git add .
$ git commit -m "commit message"
```

* Deploy on heroku
```
$ git push heroku master
```

## Stack & dependecies

Project bootstraped from `create-react-app` originaly with `Codesandbox` React template. 
React Version - `v16.13`

**Dependencies**:
* React Router v5 - `react-router-dom`
* Node Sass - `node-sass`
* React Debounce Input v3 - `react-debounce-input`
* `react-html-converter` (*
* `react-element-to-jsx-string` (*

*) Could be useless and consider delete it.

## Inspirations
User Interfaces

**Office 365 apps**:
* Power Automate (previously _Flow_)
* Planner
* OneDrive

## Licence

Customer Portal™ – Advisor Systems limited :-)